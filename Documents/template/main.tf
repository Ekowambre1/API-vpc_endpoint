provider "github" {}

terraform {
  required_version = "~> 1.0"

  required_providers {
    github = {
      source  = "integrations/github"
      version = "~> 5.0"
    }
  }
}

resource "github_repository" "template" {
  name        = "template"
  description = "My awesome codebase"
  auto_init   = true

  branch_protection {
    development = {
      pattern        = "development"
      enforce_admins = true
      required_status_checks = {
        strict = true
        contexts = [
          "default"
        ]
      }
      required_pull_request_reviews = {
        required_approving_review_count = 1
        dismiss_stale_reviews           = true
      }
      restrictions = null
    }

    main = {
      pattern        = "main"
      enforce_admins = true
      required_status_checks = {
        strict = true
        contexts = [
          "default"
        ]
      }
      required_pull_request_reviews = {
        required_approving_review_count = 1
        dismiss_stale_reviews           = true
      }
      restrictions = null
    }
  }
}


# Creating a branch called Develpoment
resource "github_branch" "development" {
  repository = github_repository.template.name
  branch     = "development"
}

# Resource to make the development branch the default branch
resource "github_branch_default" "default" {
  repository = github_repository.template.name
  branch     = github_branch.development.branch
}

# Creating the main branch 
resource "github_branch" "main" {
  repository = github_repository.template.name
  branch     = "main"
}


# Creating the feature branch 
resource "github_branch" "feature" {
  repository = github_repository.template.name
  branch     = "feature"
}

# Enable branch protection on the development branch
resource "github_branch_protection" "development_protection" {
  repository_id                   = github_repository.template.id
  branch                          = github_branch.development.branch
  dismiss_stale_reviews           = true
  require_code_owner_reviews      = true
  required_approving_review_count = 1
}

# Resource block for the pull request
resource "github_repository_pull_request" "template-pull-request" {
  base_repository = "template"
  base_ref        = "development"
  head_ref        = "feature-branch"
  title           = "My newest feature"
  body            = "This will change everything"
}


# Define the teams and add them to the repository
resource "github_team" "engineering_team" {
  name        = "engineering_team"
  description = "engineering team"
  privacy     = "closed"
}

resource "github_team_repository" "engineers_team" {
  team_id    = github_team.engineers.id
  repository = github_repository.template.name
  permission = "push"
}


resource "github_team" "codeowners" {
  name        = "codeowners"
  description = "Mandatory approver team for pull requests"
  privacy     = "closed"
}

resource "github_team_repository" "codeowners_team" {
  team_id    = github_team.codeowners.id
  repository = github_repository.template.name
  permission = "pull"
}


resource "github_team" "architects" {
  name        = "architects"
  description = "Architects team"
  privacy     = "closed"
}

resource "github_team_repository" "architects_team" {
  team_id    = github_team.architects.id
  repository = github_repository.template.name
  permission = "pull"
}


# Define a pull request template and enforce it
resource "github_repository_pull_request_template" "my_template" {
  repository = github_repository.template.name
  template   = <<-EOF
## What does this PR do?
- 
## What are the relevant tickets?
- 
## Screenshots (if applicable)
-
## Notes for Reviewers
-
  EOF
}

resource "github_repository_pull_request_review" "codeowners_review" {
  repository                      = github_repository.template.name
  dismiss_stale_reviews           = true
  require_code_owner_reviews      = true
  required_approving_review_count = 1
}


