# Define a workflow for Terraform
resource "github_actions_workflow" "terraform" {
  name     = "Terraform Workflow"
  on       = "push"
  resolves = ["terraform-apply"]
}

#Create GitHub Secrets

resource "github_actions_secret" "aws_credentials" {
  repository        = github_repository.template.name
  secret_key        = "AWS_CREDENTIALS"
  secret_access_key = ""
  plaintext_value   = "my-secret-value"
}


#Create Teams and add them to the repository

resource "github_team_repository" "codeowners_repo" {
  team_id    = github_team.codeowners.id
  repository = github_repository.template.name
  permission = "pull"
}

resource "github_team" "engineers" {
  name    = "Engineers"
  privacy = "closed"
}

resource "github_team_repository" "engineers_repo" {
  team_id    = github_team.engineers.id
  repository = github_repository.template.name
  permission = "push"
}

resource "github_team_repository" "architects_repo" {
  team_id    = github_team.architects.id
  repository = github_repository.template.name
  permission = "push"
}

resource "github_branch_protection" "example" {
  repository = github_repository.template.name
  branch     = "main"

  required_pull_request_reviews {
    required_approving_review_count = 1
    dismiss_stale_reviews           = true
    dismissal_teams                 = [github_team.codeowners.id]
  }

  required_status_checks {
    strict   = true
    contexts = ["default"]
  }
}






